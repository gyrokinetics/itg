module mhd_data

  implicit none

  real :: time, dt, dt0, rdiff_0

  integer :: nstep, nwrite, ncycle, nprint, iexp

contains

  subroutine input

    use grid, only: nffty, nfftx, x0, y0
    
    namelist/inputs/nfftx, nffty, x0, y0, dt0, nstep, nwrite, rdiff_0, iexp

    nfftx = 64
    nffty = 64
    x0 = 1.
    y0 = 1.
    dt0 = 5.e-4
    nstep = 2000
    nwrite = 5
    rdiff_0 = 0.02

    iexp=2

!
! read namelist
!
    open(unit=12,file='mhd.in',status='old')
    read(12,inputs) ;   close(unit=12)
    close(12)
    
    dt = dt0
    nprint = 0

  end subroutine input

end module mhd_data
