module fields


  use gputest    ! for gputest only
  use devObject, only: devVar, alloc_dv, use_gpu, devf_zeros

  implicit none

  complex, dimension(:,:), allocatable :: potential, density, tperp
  real, dimension(:,:,:), allocatable :: psp
  real, dimension(:), allocatable :: phitot, timo
 
  type(devVar) :: dv_potential, dv_density, dv_tperp             
                                                      

contains

  subroutine alloc_fields (n)
        
    use gryffin_grid, only: malias, nalias
    integer, intent (in) :: n

    if(allocated(density))  deallocate(density)  ; allocate (density(malias, nalias))  ;  density = 0.
    if(allocated(tperp))    deallocate(tperp)    ; allocate (tperp(malias, nalias))    ;  tperp = 0. 
    if(allocated(potential))deallocate(potential); allocate (potential(malias, nalias));  potential = 0.

    if(allocated(phitot)) deallocate(phitot) ;   allocate (phitot(n))  ;  phitot = 0.
    if(allocated(timo)) deallocate(timo) ;       allocate (timo(n))    ;  timo = 0.
 
 !___________Nail's block____________________________________________________________________
    if (use_gpu) then
       dv_density=     alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_density)    !
       dv_tperp=       alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_tperp)      !
       dv_potential=   alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_potential)  !
    end if
 !___________________________________________________________________________________________!  

  end subroutine alloc_fields

end module fields


