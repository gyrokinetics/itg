module itg_data

  implicit none

  real :: time_, dt, dt0, rdiff_0
  real, dimension(:,:), allocatable :: rdiff

  real :: rmu1, tol, tiovte, pinput1, pinput2, pmag

  integer :: minput
  integer :: iflr, nstep, ifilter, nwrite, ncycle, nprint, iexp

contains

  subroutine input

    use gputest   ! for gputest only
    
    use gryffin_grid, only: nffty, nfftx, x0, y0
    
    namelist/inputs/nfftx, nffty, x0, y0, dt0, nstep, nwrite, tiovte, iflr, pmag, rdiff_0


    nfftx = 16
    nffty = 16
    x0 = 50.
    y0 = 50.
    dt0 = 5.0
    nstep = 500
    nwrite = 10
    tiovte = 1.0
    iflr = -2
    pmag = 1.e-5
    rdiff_0 = 0.5
    

    pinput1 = 1.
    pinput2 = 1.
    minput=1
    iexp=2	! used in moment filter
    ifilter=0

!
! read namelist
!
!    open(unit=12,file='itg.in',status='old')
!    read(12,inputs) ;   close(unit=12)
!    close(12)
    
    dt = dt0
    nprint = 0

    !allocate (rdiff(malias, nalias))
    ! Nail: malias and nalias are not defined in this module!
    if(allocated(rdiff)) deallocate(rdiff)
    allocate (rdiff(nffty,nfftx))
       
    rmu1=0. !1 !Nail: !rmu1 is not defined!  I put arbitrarily rmu1=0.1 !1.e-3    ! NAIL:  IGNORE THIS LINE
    rdiff=rdiff_0                                                                    ! NAIL: CHANGE

  end subroutine input

end module itg_data

