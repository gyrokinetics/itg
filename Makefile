EXECUTABLE	:= itg
CUFILES         := DevFunc_kernel.cu
CCFILES         := c_FGPU.cpp
CCFILES         += c_DevFunc_v5.cpp
CFILES          := fortran.c

F90FLAGS = 
FLIBS = -L/usr/local/lib -lrfftw -lfftw
ifeq ($(debug),on)
F90FLAGS += -g -implicitnone -warn all -check bounds -traceback
else
F90FLAGS += -O3 -assume nounderscore
endif

F90FILES := itg.f90 itg_data.f90 devObject.f90 fields.f90 \
	gputest.f90 gryffin_grid.f90 constants.f90 FGPU.f90 \
	itg_device_func.f90 nlps.f90 linear.f90 \
	FFTRadix4.f90 fft_work.f90 


MOD= constants.o itg_data.o gryffin_grid.o linear.o nlps.o \
	fields.o fft_work.o devObject.o FGPU.o FFTRadix44.o fft_work.o

include common.mk

#itg: itg.f90 c_DevFunc_v5.cpp c_FGPU.cpp DevFunc_kernel.cu
$(OBJDIR)/itg.f90_o: $(OBJDIR)/gputest.f90_o $(OBJDIR)/itg_data.f90_o \
	$(OBJDIR)/fields.f90_o $(OBJDIR)/linear.f90_o $(OBJDIR)/nlps.f90_o \
	$(OBJDIR)/gryffin_grid.f90_o $(OBJDIR)/constants.f90_o \
	$(OBJDIR)/devObject.f90_o $(OBJDIR)/itg_device_func.f90_o 
$(OBJDIR)/itg_data.f90_o: $(OBJDIR)/gputest.f90_o $(OBJDIR)/gryffin_grid.f90_o
$(OBJDIR)/fields.f90_o: $(OBJDIR)/gputest.f90_o $(OBJDIR)/devObject.f90_o $(OBJDIR)/gryffin_grid.f90_o
$(OBJDIR)/linear.f90_o: $(OBJDIR)/devObject.f90_o $(OBJDIR)/itg_data.f90_o $(OBJDIR)/gryffin_grid.f90_o 
$(OBJDIR)/nlps.f90_o: $(OBJDIR)/devObject.f90_o $(OBJDIR)/constants.f90_o \
	$(OBJDIR)/gryffin_grid.f90_o $(OBJDIR)/itg_device_func.f90_o \
	$(OBJDIR)/FFTRadix4.f90_o $(OBJDIR)/fft_work.f90_o 
$(OBJDIR)/gputest.f90_o: $(OBJDIR)/devObject.f90_o 
$(OBJDIR)/itg_device_func.f90_o: $(OBJDIR)/devObject.f90_o
$(OBJDIR)/devObject.f90_o: $(OBJDIR)/fortran.c_o $(OBJDIR)/FGPU.f90_o
$(OBJDIR)/itg.f90_o: $(OBJDIR)/DevFunc_kernel.cu_o
$(OBJDIR)/FFTRadix4.f90_o: $(OBJDIR)/fft_work.f90_o

#.SUFFIXES: .f90 .o
#
#.f90.o:
#	$(F90) $(F90FLAGS) -c $<





