!____Nail's module for customized device functions used in itg
!
!            all functions used in itg are implemented as function of class 
!
!            devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal,cmplxhandling),   
!
!            which asumes dense storage: matrix sizes coinside with leading dimensions   
!

module itg_device_func

  use devObject

  implicit none

contains

  !-------------------------------------------------------------------------------------------    

  subroutine devcust_itg_prop(od,ot,op,d,nld1,nld2,tp,nlt1,nw,tw,h)    

    !         puts group of operators in RK propagation on the GPU

    !               od=d-h*(nld1+nld2)
    !               ot=tp-h*nlt1
    !               op=od*nw+ot*tw    

    !         h-is a real constant, other are complex device variables,
    !         except of nw and tw, which are real


    implicit none

    type(devVar) od,ot,op,d,nld1,nld2,tp,nlt1,nw,tw
    real h
    type(devVar) devVars(10)
    integer(4) fPtr,ndevvars/10/,nparint/0/,nparreal/1/
    integer(4) parint(1)
    real(4) parreal(1)

    devVars(1)=od
    devVars(2)=ot
    devVars(3)=op
    devVars(4)=d
    devVars(5)=nld1
    devVars(6)=nld2
    devVars(7)=tp
    devVars(8)=nlt1
    devVars(9)=nw
    devVars(10)=tw
    parreal(1)=-h

    call cf_getdevfuncptr('custom_itg_prop',fPtr)
    call devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal)

  end subroutine devcust_itg_prop

  subroutine devcust_mhd_prop(ov,oz,v,n,k2i,h)

    !         puts group of operators in RK propagation on the GPU

    !           ov1 = v1 - h*n1
    !           ov2 = v2 - h*n2
    !           oz1 = -ov1*k2inv
    !	        oz2 = -ov2*k2inv

    implicit none

    type(devVar), dimension(2) :: ov,oz,v,n
    type(devVar) :: k2i
    real h
    type(devVar), dimension(9) :: devVars
    integer(4) fPtr,ndevvars/7/,nparint/0/,nparreal/1/
    integer(4) parint(1)
    real(4) parreal(1)

    devVars(1)=ov(1)
    devVars(2)=ov(2)
    devVars(3)=oz(1)
    devVars(4)=oz(2)
    devVars(5)=v(1)
    devVars(6)=v(2)
    devVars(7)=n(1)
    devVars(8)=n(2)
    devVars(9)=-k2i ! note negative sign
    parreal(1)=-h   ! note negative sign

    call cf_getdevfuncptr('custom_mhd_prop',fPtr)
    call devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal)

  end subroutine devcust_mhd_prop

  !-------------------------------------------------------------------------------------------    

  subroutine devcust_itg_hadam2(a,ikx,kxa2,iky,kya2)  

    !           puts the following operation on the GPU
    !           also can be realized (maybe a bit slower) by calling twice intrinsic devObject function dev_hadamardf with option 'i'
    !           
    !               kxa2 = ikx*a           ! call devf_hadamardf(kxa2,a,ikx,'i')
    !               kya2 = iky*a           ! call devf_hadamardf(kya2,a,iky,'i')
    !                       
    !           ikx and iky are purely imaginary stored as real
    !           a,kxa2,kya2 are complex
    !             

    implicit none

    type(devVar) a,ikx,kxa2,iky,kya2
    type(devVar) devVars(5)
    integer(4) fPtr,ndevvars/5/,nparint/0/,nparreal/0/
    integer(4) parint(2)
    real(4) parreal(1)

    devVars(1)=a
    devVars(2)=ikx
    devVars(3)=kxa2
    devVars(4)=iky
    devVars(5)=kya2

    call cf_getdevfuncptr('custom_itg_hadam2',fPtr)
    call devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal)

  end subroutine devcust_itg_hadam2

  !-------------------------------------------------------------------------------------------    

  subroutine devcust_itg_det2 (nl_term,kxa2,kxb2,kya2,kyb2)   
    !       
    !           puts the following operation (pointwise 2x2 determinant) on the GPU 
    !           
    !               nl_term = kxa2 * kyb2 - kxb2 * kya2

    !            nl_term,kxa2,kxb2,kya2,kyb2 are complex

    implicit none

    type(devVar) nl_term,kxa2,kxb2,kya2,kyb2
    type(devVar) devVars(5)
    integer(4) fPtr,ndevvars/5/,nparint/0/,nparreal/0/
    integer(4) parint(1)
    real(4) parreal(1)

    devVars(1)=nl_term
    devVars(2)=kxa2
    devVars(3)=kxb2
    devVars(4)=kya2
    devVars(5)=kyb2

    call cf_getdevfuncptr('custom_itg_det2',fPtr)
    call devf_gen_pointwise_full(fPtr,ndevvars,nparint,nparreal,devVars,parint,parreal)

  end subroutine devcust_itg_det2

  !-------------------------------------------------------------------------------------------    

end module itg_device_func
