module gryffin_grid
!
! This module contains routines and variables related to the computational grid.
!
  implicit none

  integer nffty	     !  # of y grid points 
  integer nfftx	     !  # of x grid points
  integer md	     ! total # of ky modes after dealiasing
  integer nd	     ! total # of kx modes after dealiasing
  integer :: nddp, nddm, malias, nalias, madd, nadd, meq, neq, mddp, mddm

  real :: x0, y0

  real, dimension(:,:), allocatable :: rkperp2 ! kperp**2 for each mode
  real, dimension(:), allocatable :: rkx, rky
  integer, dimension(:), allocatable :: mrr, nrr

contains

  subroutine init_grid 

    integer :: m, n

    malias=nffty
    md=2*((nffty-1)/3)+1
    
    nalias=nfftx
    nd=2*((nfftx-1)/3)+1
    
    nddm=nd/2
    nddp=nd/2+1

    mddm=md/2
    mddp=md/2+1

!     
!     Set up for complex-to-complex x FFT's.
!     nd = # of independent kx modes (usually odd for symmetry around kx=0)
!     nalias = # of x points (with dealiasing):

    nadd=nalias-nd
    madd=malias-md

    call alloc_grid

    do n=1,nalias/2+1
       nrr(n)=n-1
    enddo
    
    do n=1,nalias/2
       nrr(n+nalias/2)=n-nalias/2-1
    enddo
    

    do m=1,malias/2+1
       mrr(m)=m-1
    enddo
    
    do m=1,malias/2
       mrr(m+malias/2)=m-malias/2-1
    enddo

    rky = real(mrr)/y0
    rkx = real(nrr)/x0

    do n=1,nalias
       do m=1,malias
          rkperp2(m,n) = rkx(n)**2 + rky(m)**2
       end do
    end do

  end subroutine init_grid
  
  subroutine alloc_grid
 
    if(allocated(mrr)) deallocate(mrr)         ;  allocate (mrr(malias))   ; mrr = 0
    if(allocated(nrr)) deallocate(nrr)         ;  allocate (nrr(nalias))   ; nrr = 0
    if(allocated(rkx)) deallocate(rkx)         ;  allocate (rkx(nalias))   ; rkx = 0.
    if(allocated(rky)) deallocate(rky)         ;  allocate (rky(malias))   ; rky = 0.
    if(allocated(rkperp2)) deallocate(rkperp2) ;  allocate (rkperp2(malias, nalias)) ; rkperp2 = 0.

  end subroutine alloc_grid

end module gryffin_grid
