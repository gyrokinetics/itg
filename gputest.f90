module gputest

  implicit none

  logical usegpu
  logical usefort

  integer :: nloop,maxloops,startsize,intloop,maxintloops,maxintloops1
  real(4) :: time_begin,time_end,time_for,time_gpu,time_for2
  real :: tol2,errtst
  real(4) ZERO/0./
  integer :: length

  complex, allocatable :: testmx(:,:)

contains

  !========================================================================

  subroutine settest(gpu,cpu)

    implicit none

    logical gpu,cpu

    usegpu=gpu 
    usefort=cpu 

    maxloops=4 !9 !6 !1 !7
    startsize=256 !256 !8192 !16384 !256
    maxintloops= 1 !1000-1 ! 000 !1024 !000!16324 !256 !000
    tol2=1.e-6
    length=startsize/2 

    if(usefort.and.usegpu)then 
       write(*,101)
       !write(*,111)
    elseif(usegpu) then
       write(*,103)
    endif

    write(*,*)

101 format('   N    ','   CPU Time(s)   ','GPU Time (s)  ',' CPU/GPU Ratio    ','      error    ')
103 format('    N    ','        GPU Time (s)       ')
111 format('   N   ','F_dbl(s) ','F_sgl(s) ',' GPU(s)','  CPU/GPU ','  err_F   ','  err_GPU   ','  err_s')

  end subroutine settest

  !=======================================================================

  subroutine gputestout(potential,dv_potential,density,dv_density,tperp,dv_tperp)

    use devObject, only: devVar

    implicit none
    complex potential(length/2,length),density(length/2,length),tperp(length/2,length)
    type(devVar) dv_potential,dv_density,dv_tperp

    real(8) dum2,err2,errdum,err3

    if(usefort.and.usegpu)then

       allocate (testmx(length/2,length))

       call compare_dv_cmplx_matrices(dv_potential,potential,errdum)
       err2=errdum
       call compare_dv_cmplx_matrices(dv_density,density,errdum)
       err2=max(err2,errdum)
       call compare_dv_cmplx_matrices(dv_tperp,tperp,errdum)
       err2=max(err2,errdum)

       dum2=0.
       if(time_gpu.ne.0.)then
          dum2=time_for/time_gpu
          write(*,102) length,time_for,time_gpu,dum2,err2
       else
          write(*,105) length,time_for,time_gpu,err2
       endif

       deallocate (testmx)

    elseif(usegpu) then
       write(*,104) length,time_gpu
    endif

102 format(i7,3e15.5,e18.3)
104 format(i7,e20.7)
105 format(i7,2e15.5,'            N/A',e18.3)
112 format(i7,4f9.5,3e11.3)
115 format(i7,3f9.5,'            N/A',3e11.3) 

  end subroutine gputestout


  !========================================================================

  subroutine compare_realvecs(vec1,vec2,lngth,tol,err)
    !compare single precision real vectors, vec2-reference

    implicit none

    real vec1(*)
    real vec2(*)
    real(8) err,tol,dum1,dum2
    real dum3
    integer i,lngth

    err=0.
    !write(*,*) lngth

    do i=1,lngth
       dum3=vec2(i)
       dum2=abs(vec1(i)-dum3)
       dum1=abs(dum3)
       !if(dum1.ge.tol) dum2=dum2/dum1
       err=max(err,dum2)
       !if(err.ge.tol) then
       !write(*,100) i,vec1(i),vec2(i),dum2
       !stop
       !endif
       !100 format(i8,3e15.7)        
    enddo

    !stop

  end subroutine compare_realvecs

  !=======================================================================

  subroutine compare_intvecs_S(vec1,vec2,lngth,tol,err)
    !compare integer vectors, vec2-reference

    implicit none

    integer vec1(*)
    integer vec2(*)
    real(8) err,tol,dum1,dum2
    real dum3
    integer i,lngth

    err=0.
    !write(*,*) length

    do i=1,lngth
       dum3=float(vec2(i))
       dum2=abs(vec1(i)-dum3)
       dum1=abs(dum3)
       if(dum1.ge.tol) dum2=dum2/dum1
       err=max(err,dum2)
       !if(err.ge.tol) then
       !write(*,100) i,vec1(i),vec2(i),err
       !stop
       !endif
100    format(i8,2i20,e15.7)        
    enddo

    !stop

  end subroutine compare_intvecs_S


  !=======================================================================

  subroutine compare_vecs_SD(vec1,vec2,lngth,tol,err)
    !compare real vectors, vec1-single precision, vec2-double precision reference vector
    implicit none

    real(4) vec1(*)
    real vec2(*)
    real(8) err,tol,dum1,dum2
    integer i,lngth

    err=0.

    do i=1,lngth
       dum1=vec2(i)
       dum2=abs(vec1(i)-dum1)
       dum1=abs(dum1)
       if(dum1.ge.tol) dum2=dum2/dum1
       err=max(err,dum2)
       !write(*,*)i,vec1(i),vec2(i),dum2
    enddo

    ! stop

  end subroutine compare_vecs_SD

  !======================================================

  subroutine L2errreal (vec1,vec2,lngth,err)

    real vec1(*),vec2(*)
    real(8) err
    integer lngth
    real(8), allocatable :: vdiff(:)

    allocate(vdiff(lngth))
    vdiff=vec1(1:lngth)-vec2(1:lngth)
    err=sqrt(dot_product(vdiff,vdiff)/lngth)
    deallocate(vdiff)

  end subroutine L2errreal

  !=======================================================================

  subroutine compare_cmplxvecs(vec1,vec2,lngth,tol,err)
    !compare complex vectors, vec2-reference

    implicit none

    complex vec1(*)
    complex vec2(*)
    real(8) err,tol,dum1,dum2
    complex(8) dum3
    integer i,lngth

    err=0.
    !write(*,*) length

    do i=1,lngth
       dum3=vec2(i)
       dum2=abs(vec1(i)-dum3)
       dum1=abs(dum3)
       if(dum1.ge.tol) dum2=dum2/dum1
       err=max(err,dum2)
       !if(err.ge.tol) then
       !write(*,100) i,vec1(i),vec2(i),err
       !stop
       !endif
100    format(i8,5e15.7)        
    enddo

    !stop

  end subroutine compare_cmplxvecs

  !======================================================================

  subroutine L2errcmplx (vec1,vec2,lngth,err)

    complex vec1(*),vec2(*)
    real(8) err
    integer lngth
    complex(8), allocatable :: vdiff(:)

    allocate(vdiff(lngth))
    vdiff=vec1(1:lngth)-vec2(1:lngth)
    err=sqrt(dot_product(vdiff,vdiff)/lngth)
    deallocate(vdiff)

  end subroutine L2errcmplx

  !=======================================================================


  subroutine compare_dv_cmplx_matrices(dv_mx1,mx2,err)
    !compare single precision complex vectors, vec2-reference

    use devObject, only: transfer_c4, devVar

    implicit none

    type(devVar) dv_mx1
    complex mx2(length/2,length)
    real(8) err,dum1,dum2
    complex(8) dum3
    integer i,j

    call transfer_c4(testmx,dv_mx1,.false.)
    testmx=testmx-mx2

    err=0.
    !write(*,*) length

    do j=1,length
       do i=1,length/2

          dum3=mx2(i,j)
          dum2=abs(testmx(i,j))
          dum1=abs(dum3)
          if(dum1.ge.tol2) dum2=dum2/dum1
          err=max(err,dum2)
          !if(err.ge.tol2) then
          !write(*,100) i,j,testmx(i,j),mx2(i,j),dum2
          !stop
          !endif
       enddo
    enddo

100 format(i8,5e15.7)

    !stop

  end subroutine compare_dv_cmplx_matrices

  !======================================================================

end module gputest

