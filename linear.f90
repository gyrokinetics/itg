module linear

  use devObject, only: devVar, transfer_r4, use_gpu, alloc_dv

  implicit none

  real, dimension(:,:), allocatable :: dfilter
  real, dimension (:,:), allocatable :: flrwgt, g0wgt, nwgt, twgt, g0wgtp

  type (devVar) dv_flrwgt, dv_g0wgt, dv_nwgt, dv_twgt


contains

  subroutine flrinit
    !     
    !     initializes some arrays used in flr, poisson, timestep, and nlpsc

    use itg_data, only: iflr, tiovte
    use itg_data, only: iexp, rdiff, dt
    use gryffin_grid, only: malias, nalias, rkperp2

    real, dimension (:,:), allocatable :: rgammas, g0, tmu, pfilter, pfilter2

    real :: rkp2
    integer :: m, n, ifail

    if(allocated(nwgt))     deallocate(nwgt)     ;  allocate (nwgt  (malias, nalias))   ; nwgt = 0.
    if(allocated(twgt))     deallocate(twgt)     ;  allocate (twgt  (malias, nalias))   ; twgt = 0.
    if(allocated(tmu))      deallocate(tmu)      ;  allocate (tmu   (malias, nalias))   ; tmu = 0.
    if(allocated(g0))       deallocate(g0)       ;  allocate (g0 (malias, nalias))      ; g0 = 0.
    if(allocated(g0wgt))    deallocate(g0wgt)    ;  allocate (g0wgt (malias, nalias))   ; g0wgt = 0.
    if(allocated(g0wgtp))   deallocate(g0wgtp)   ;  allocate (g0wgtp(malias, nalias))   ; g0wgtp = 0.
    if(allocated(pfilter))  deallocate(pfilter)  ;  allocate (pfilter(malias, nalias))  ; pfilter = 0.
    if(allocated(pfilter2)) deallocate(pfilter2) ;  allocate (pfilter2(malias, nalias)) ; pfilter2 = 0.
    if(allocated(rgammas))  deallocate(rgammas)  ;  allocate (rgammas(malias, nalias))  ; rgammas = 0.
    if(allocated(dfilter))  deallocate(dfilter)  ;  allocate (dfilter(malias, nalias))  ; dfilter = 0.
    if(allocated(flrwgt))   deallocate(flrwgt)   ;  allocate (flrwgt(malias,nalias))  ! Nail: this array was not allocated, but used!- I have to add this statement

    do n=1,nalias
       do m=1,malias
          rkp2=rkperp2(m,n)
          rgammas(m,n) = s18cfE(rkp2,ifail)/s18ceE(rkp2,ifail)
          g0 =s18ceE(rkp2,ifail)
       end do
    end do

    !     
    !     Use Rogers' approximation if iflr=-1:
    !     
    !    if (iflr == -1) then   ! NAIL: CHANGE
    if (iflr < 0) then

       pfilter2 = rkperp2
       g0wgt =1.0          
       g0wgtp =1.0         
       !       flrwgt =-rkperp2/2. 
       !       twgt =-rkperp2/2.   
       flrwgt =-rkperp2/2. * tiovte   ! NAIL:  CHANGE
       twgt =-rkperp2/2. * tiovte     ! NAIL:  CHANGE
       nwgt =1.            
       !     
       !     Use drift kinetic approximation if iflr=0:
       !     
    else if (iflr == 0) then

       pfilter2 = 0.
       g0wgt =1.
       g0wgtp =1.
       flrwgt =0.
       twgt =0.
       nwgt =1.
       !     
       !     Use "new" Pade approximation if iflr=1:
       !     
    else if (iflr == 1) then

       pfilter2 = (1.-1./(1.+rkperp2))
       g0wgt =1./(1.+rkperp2/2.)
       g0wgtp =2./(2.+rkperp2)
       flrwgt =-rkperp2/(2.+rkperp2)
       twgt =-rkperp2/(2.+rkperp2)
       nwgt =1.
       !     
       !     or use "new" FLR model for iflr=2 or 3:
       !     
    else if(iflr == 2.or.iflr == 3 .or. iflr==8 .or. iflr==9) then

       pfilter2 = 1-g0
       g0wgt = sqrt(g0)
       flrwgt =-rkperp2/2.*(1.-rgammas)
       tmu=1.-rkperp2*(1.-rkperp2/2.)+rkperp2/2.*rgammas*(1.-rkperp2*rgammas)
       twgt =-rkperp2/2.*(1.-rgammas)
       nwgt =1.-rkperp2*(1.-rkperp2/4.)+rkperp2*rgammas/2.*(1.+rkperp2*(1.-3.*rgammas/2.))
       g0wgtp =g0/tmu
       !     
       !     <J_0> = exp(-b/2) for iflr=4:
       !     
    else if(iflr == 4) then

       pfilter2 = 1.-g0
       g0wgt =exp(-rkperp2/2.)
       g0wgtp =exp(-rkperp2/2.)
       flrwgt =-rkperp2/2.
       twgt =-rkperp2/2.
       nwgt =1.
       !     
       !     or use Taylor series:
       !     
    else if(iflr == 5) then

       !       g0wgt =1.0
       !       g0wgtp =1.
       pfilter2 = rkperp2
       g0wgt =1.0-rkperp2/2.
       g0wgtp =1.-rkperp2/2.
       flrwgt =-rkperp2/2.
       twgt =-rkperp2/2.
       nwgt =1.
       !     
       !     Like Ron, but Linsker treatment different
       !     
    else if(iflr == 6) then

       pfilter2 = 1.-g0
       g0wgt =g0
       flrwgt =-rkperp2*(1.-rgammas)
       g0wgtp =1.
       twgt =0.0
       nwgt =1.
       !
       !      Full FLR, but no Linsker effect
       !     
    else if(iflr == 7) then

       pfilter2 = 1.-g0
       g0wgt = g0
       flrwgt = -rkperp2*(1.-rgammas)
       twgt =0.0
       nwgt =1.
       g0wgtp =1.
       !
       !     Pade in Poisson with new toroidal FLR terms
       !
    else if(iflr == 8.or.iflr == 9) then

       pfilter2 = 1.-g0
       g0wgt =1.-g0
       g0wgtp =1.
       twgt =-rkperp2/2./(1.+rkperp2/2.)**2
       nwgt =1./(1.+rkperp2/2.)

    else if(iflr == 10) then

       pfilter2 = rkperp2
       g0wgt   = 1.
       g0wgtp  = 1.
       flrwgt  = 0.
       twgt   = 0.
       nwgt   = 1.
    endif

    flrwgt=flrwgt*g0wgt

    if (iflr == -1) then
       !       pfilter = 1.0/(1+2.*pfilter2)
       pfilter = 1.0/(1.0+(1.+tiovte)*pfilter2)   ! NAIL: CHANGE
    else if (iflr == -2) then                     ! NAIL: CHANGE
       pfilter = 1.0/(1.+(1.+tiovte)*pfilter2)    ! NAIL: CHANGE
       pfilter (1,2:) = 1.0/rkperp2(1,2:)         ! NAIL: CHANGE
       pfilter (1,1) = 0.                         ! NAIL: CHANGE
    else
       pfilter = 1.0/(tiovte+pfilter2)
    endif

    ! HM only: 
    g0wgtp = g0wgtp * pfilter
    nwgt = nwgt * g0wgtp
    twgt = twgt * g0wgtp

    !write (*,*) 'dt = ',dt
    !dfilter = 1./(1.+dt*rdiff*rkperp2**iexp)
    do n=1,nalias
       do m=1,malias
          dfilter(m,n)=1./(1.+dt*rdiff(m,n)*rkperp2(m,n)**iexp)
       enddo
    enddo

    deallocate (pfilter, pfilter2, rkperp2, g0, tmu, rgammas)


    if (use_gpu) then
       !_______________Nail's block_____________________
       dv_flrwgt=     alloc_dv('real',malias,nalias)    !
       dv_g0wgt=      alloc_dv('real',malias,nalias)    !
       dv_nwgt=       alloc_dv('real',malias,nalias)    !
       dv_twgt=       alloc_dv('real',malias,nalias)    !
       !________________________________________________!  
       
       
       !_______________Nail's block____________________
       !
       call transfer_r4 (flrwgt,dv_flrwgt,.true.)         !        
       call transfer_r4 (g0wgt, dv_g0wgt, .true.)         !        
       call transfer_r4 (nwgt,  dv_nwgt,  .true.)         !    
       call transfer_r4 (twgt,  dv_twgt,  .true.)         !    
       !_______________________________________________!  
    end if

    ! Nail: if flrwgt,g0wgt,nwgt,twgt are not in use except of the functions in main, 
    ! they can be deallocated after data transferred in GPU              
    

  end subroutine flrinit

  !--------------------------------------------------------------------


  subroutine filter(density1)

    use gryffin_grid, only: rkperp2

    complex, dimension(:,:), intent (in out) :: density1

    density1 = density1 * dfilter

  end subroutine filter

  ! modified Bessel function I_0*exp(-x)
  real function s18cee(rkp2,ifail)
    implicit none
    real :: rkp2, xdum
    integer :: ifail

    call gamma_n(rkp2,s18cee,xdum)

  end function s18cee

  ! modified Bessel function I_1*exp(-x)
  real function s18cfe(rkp2,ifail)
    implicit none

    real :: rkp2, xdum
    integer :: ifail

    call gamma_n(rkp2,xdum,s18cfe)

  end function s18cfe

  subroutine gamma_n(x_LL,g0_LL,g1_LL)

    !cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
    ! Evaluates modified Bessel functions
    !
    !cmnt  gamma_n(x,g0,g1) returns g0=exp(-abs(x))*I(0)(x)
    !cmnt  gamma_n(x,g0,g1) returns g1=exp(-abs(x))*I(1)(x)
    ! (from Kerbel's port of the code).
    !cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


    integer k
    real :: tolerance
    !      parameter(tolerance=1.0e-16)

    real :: xk,xki,xkp1i,xkp2i,error1,error0

    real :: x_LL, g0_LL, g1_LL
    real :: tk0_LL, tk1_LL, xb2_LL, xb2sq_LL

    !ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
    !cmnt  compute Gamma_0 and Gamma_1
    !ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

    tolerance=epsilon(1.0)/8.0

    if (x_LL == 0.) then
       g0_LL=1.
       g1_LL=0.
       return
    endif

    xb2_LL=.5*x_LL  
    xb2sq_LL=xb2_LL**2
    tk0_LL=exp(-x_LL)
    tk1_LL=tk0_LL*xb2_LL
    g0_LL=tk0_LL
    g1_LL=tk1_LL

    xk=1.

10  continue

    xki=1./xk
    xkp1i=1./(xk+1.)
    xkp2i=1./(xk+2.)
    tk0_LL=tk0_LL*xb2sq_LL*xki*xki
    tk1_LL=tk1_LL*xb2sq_LL*xki*xkp1i
    g0_LL=g0_LL+tk0_LL
    g1_LL=g1_LL+tk1_LL
    xk=xk+1.
    error0=abs(tk0_LL/g0_LL)
    error1=abs(tk1_LL/g1_LL)
    if (error0 > tolerance .or. error1 > tolerance) goto 10

  end subroutine gamma_n

end module linear






