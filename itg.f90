program itg

!  use IFPORT          ! for wall clock time measurement only
  use gputest, only: settest, length, nloop, maxloops, gputestout

  use itg_data, only: input, nprint, nwrite, dt, time_, nstep
  use fields, only: potential, density, tperp, phitot, psp, timo
  use linear, only: nwgt, twgt, g0wgt, flrwgt, flrinit
  use nlps, only: nlpsc
  use gryffin_grid, only: malias, nalias
  use constants, only: debug


  !_______Nail's block________________________________________
  !
  use devObject, only: hadamard, devVar, open_devObjects, close_devObjects, transfer_c4
  use devObject, only: use_gpu
  use itg_device_func, only: devcust_itg_prop
  use fields, only: dv_potential,dv_density,dv_tperp      
  use linear, only: dv_nwgt,dv_twgt,dv_g0wgt,dv_flrwgt    
  !___________________________________________________________!  

  implicit none


  !_______Nail's block__________________________________________!
  type(devVar) :: dv_nl_density1, dv_nl_density2, dv_nl_tperp1  !
  type(devVar) :: dv_old_density, dv_old_tperp, dv_old_potential!
  type(devVar) :: dv_phi_u, dv_phi_flr1                         !
  !_____________________________________________________________!    


  complex, dimension(:,:), allocatable :: nl_density1, nl_density2, nl_tperp1
  complex, dimension(:,:), allocatable :: old_density, old_tperp, old_potential
  complex, dimension(:,:), allocatable :: phi_u, phi_flr1

  real :: hdt
  integer :: n_time_diag, ncycle, m, n


  !_______Nail's block_________
  !
  call open_devObjects        
  !
  !___________________________

  call settest(.true.,.false.)    ! (gpu,cpu)    ! set the performance test

  !----------------------------Main Test Loop Size Varies


  DO nloop=1,maxloops
     length=2*length

     !
     ! Read namelists and set defaults
     call input
     if (debug) write (*,*) 'finished input'

     ! Set up arrays, constants, and ic's
     call init
     if (debug) write (*,*) 'finished init'

     ! Allocate local arrays
     call allocate_local 
     if (debug) write (*,*) 'finished allocate_local'

     time_ = 0.   ! function time is a standard function in IFPORT and can't be used, if time is measured

     !
     ! ****************************************************
     !                     Top of the loop
     ! ****************************************************


!     time_gpu=secnds(ZERO)  ! must be some IFPORT procedure?

     do ncycle = 0, nstep-1

        time_ = time_ + dt
        hdt = 0.5*dt

!        write(*,*) ncycle,nstep

        if (use_gpu) then
           call hadamard (dv_phi_u,   dv_potential, dv_g0wgt)          
           call hadamard (dv_phi_flr1,dv_potential, dv_flrwgt) 
        else
           call hadamard (phi_u,    potential, g0wgt)
           call hadamard (phi_flr1, potential, flrwgt)
        end if
           
        if (use_gpu) then
           call nlpsc(0, dv_phi_u,     dv_density, dv_nl_density1)          
           call nlpsc(1, dv_phi_u,     dv_tperp,   dv_nl_tperp1)        
           call nlpsc(2, dv_phi_flr1,  dv_tperp,   dv_nl_density2) 
        else
           call nlpsc(0, phi_u,     density, nl_density1)          
           call nlpsc(1, phi_u,     tperp,   nl_tperp1)        
           call nlpsc(2, phi_flr1,  tperp,   nl_density2) 
        end if

        if (use_gpu) then
           call devcust_itg_prop(dv_old_density,dv_old_tperp,dv_old_potential, &
                &                       dv_density,dv_nl_density1,dv_nl_density2,dv_tperp,dv_nl_tperp1,dv_nwgt,dv_twgt,hdt)    
        else
           old_density = density - hdt*(nl_density1 + nl_density2)
           old_tperp   = tperp   - hdt* nl_tperp1
           old_potential = old_density*nwgt + old_tperp*twgt
        end if

        if (use_gpu) then
           call hadamard(dv_phi_u,   dv_old_potential,   dv_g0wgt)                          
           call hadamard(dv_phi_flr1,dv_old_potential,   dv_flrwgt)       
        else
           call hadamard(phi_u,   old_potential, g0wgt)                          
           call hadamard(phi_flr1,old_potential, flrwgt)       
        end if

        if (use_gpu) then
           call nlpsc(0, dv_phi_u,     dv_old_density, dv_nl_density1)                                                            
           call nlpsc(1, dv_phi_u,     dv_old_tperp,   dv_nl_tperp1)                                                              
           call nlpsc(2, dv_phi_flr1,  dv_old_tperp,   dv_nl_density2)
        else
           call nlpsc(0, phi_u,     old_density, nl_density1)                                                            
           call nlpsc(1, phi_u,     old_tperp,   nl_tperp1)                                                              
           call nlpsc(2, phi_flr1,  old_tperp,   nl_density2)
        end if

        if (use_gpu) then
           call devcust_itg_prop(dv_density,dv_tperp,dv_potential,dv_density, &
                &                       dv_nl_density1,dv_nl_density2,dv_tperp,dv_nl_tperp1,dv_nwgt,dv_twgt,dt)    
        else
           density = density - dt*(nl_density1 + nl_density2)
           tperp   = tperp   - dt* nl_tperp1
           potential = density*nwgt + tperp*twgt
        end if

        if(.false.) then     ! change to .true. if output from each step should be transferred to cpu      

           if(mod(ncycle,nwrite) == nwrite-1) then
              nprint = nprint + 1
              if (use_gpu) call transfer_c4(potential,dv_potential,.false.)          

              phitot(nprint) = sum(cabs(potential))
              timo(nprint)=time_

           endif
        endif

     enddo

!     time_gpu=secnds(time_gpu)

     ! ****************************************************
     !                     Bottom of the loop
     ! ****************************************************
     call gputestout(potential,dv_potential,density,dv_density,tperp,dv_tperp)
     call wpunch

     if (use_gpu) then
        call dev_deallocate
     else
        call deallocate_all   
     end if

  ENDDO               ! end of size variation loop

  !_______Nail's block______________                          ! Nail: that's it!
  !
  call close_devObjects         !
  !_________________________________!  


contains

  subroutine init
    !     
    !     Set up arrays and define values of constants.
    !
    use linear, only: flrinit
    use nlps, only: init_nlps
    use fields, only: alloc_fields
    use gryffin_grid, only: init_grid

    ! Initialize grid quantities
    call init_grid
    if (debug) write (*,*) 'finished init_grid'

    ! Allocate main 3-D arrays and some diagnostic arrays
    call alloc_fields (nstep/nwrite+1)
    if (debug) write (*,*) 'finished alloc_fields'

    ! Initialize fft's for nonlinear terms
    call init_nlps
    if (debug) write (*,*) 'finished init_nlps'

    ! Initialize the main fields
    call pertb  
    if (debug) write (*,*) 'finished pertb'

    ! Initialize weights for FLR terms
    call flrinit
    if (debug) write (*,*) 'finished flrinit'

  end subroutine init

  !---------------------------------------------------------------------------------------  

  subroutine allocate_local

    use gryffin_grid, only: malias, nalias
    use devObject, only: alloc_dv, devf_zeros


    if (use_gpu) then
       !___________Nail's block________________________________________________________________________
       !
       dv_old_density=     alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_old_density)   !
       dv_old_tperp=       alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_old_tperp)     !
       dv_old_potential=   alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_old_potential) !
       dv_phi_u=           alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_phi_u)         !
       dv_phi_flr1=        alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_phi_flr1)      !
       dv_nl_density1=     alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_nl_density1)   !
       dv_nl_density2=     alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_nl_density2)   !
       dv_nl_tperp1=       alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_nl_tperp1)     !
       !______________________________________________________________________________________________!
    else       
       if(allocated(old_density)) deallocate(old_density)     ;  allocate (old_density(malias, nalias))   ; old_density = 0.
       if(allocated(old_tperp)) deallocate(old_tperp)         ;  allocate (old_tperp(malias, nalias))     ; old_tperp = 0.
       if(allocated(old_potential)) deallocate(old_potential) ; allocate (old_potential (malias, nalias)) ; old_potential = 0.
       if(allocated(phi_u)) deallocate(phi_u)                 ; allocate (phi_u(malias, nalias))          ; phi_u = 0.
       if(allocated(phi_flr1)) deallocate(phi_flr1)           ; allocate (phi_flr1(malias, nalias))       ; phi_flr1 = 0. 
       if(allocated(nl_density1)) deallocate(nl_density1)     ; allocate (nl_density1(malias, nalias))    ; nl_density1 = 0.
       if(allocated(nl_density2)) deallocate(nl_density2)     ; allocate (nl_density2(malias, nalias))    ; nl_density2 = 0.
       if(allocated(nl_tperp1)) deallocate(nl_tperp1)         ; allocate (nl_tperp1(malias, nalias))      ; nl_tperp1 = 0.
    end if

  end subroutine allocate_local

  !=====================================================Nail's deallocation subroutine=====

  subroutine dev_deallocate

    use nlps!, only: fftplan,dv_scalefac,dv_ikx,dv_iky,dv_kxa2,dv_kya2,dv_kxb2,dv_kyb2 
    use devObject, only: devf_destroyfftplan, dealloc_dv

    implicit none

    call devf_destroyfftplan(fftplan)

    call dealloc_dv(dv_scalefac)

    call dealloc_dv(dv_ikx)
    call dealloc_dv(dv_iky)

    call dealloc_dv(dv_kxa2)
    call dealloc_dv(dv_kya2)
    call dealloc_dv(dv_kxb2)
    call dealloc_dv(dv_kyb2)

    call dealloc_dv(dv_flrwgt)
    call dealloc_dv(dv_g0wgt)
    call dealloc_dv(dv_nwgt)
    call dealloc_dv(dv_twgt)

    call dealloc_dv(dv_density)
    call dealloc_dv(dv_tperp)
    call dealloc_dv(dv_potential)

    call dealloc_dv(dv_old_density)
    call dealloc_dv(dv_old_tperp)
    call dealloc_dv(dv_old_potential)
    call dealloc_dv(dv_phi_u)
    call dealloc_dv(dv_phi_flr1)
    call dealloc_dv(dv_nl_density1)
    call dealloc_dv(dv_nl_density2)
    call dealloc_dv(dv_nl_tperp1)

  end subroutine dev_deallocate

  !========================================================================================
  ! fortran deallocation (to do: check if all allocatables are deallocated!)

  subroutine deallocate_all

    use nlps
    use gryffin_grid
    use linear

    implicit none

    deallocate(scalefac)

    deallocate(ikx)
    deallocate(iky)

    deallocate(kxa2)
    deallocate(kya2)
    deallocate(kxb2)
    deallocate(kyb2)

    deallocate(flrwgt)
    deallocate(g0wgt)
    deallocate(nwgt)
    deallocate(twgt)
    deallocate(g0wgtp)

    deallocate(dfilter)

    deallocate(density)
    deallocate(tperp)
    deallocate(potential)
    deallocate(psp)
    deallocate(phitot)
    deallocate(timo)

    deallocate(mrr)
    deallocate(nrr)
    deallocate(rkx)
    deallocate(rky)

    deallocate(old_density)
    deallocate(old_tperp)
    deallocate(old_potential)
    deallocate(phi_u)
    deallocate(phi_flr1)
    deallocate(nl_density1)
    deallocate(nl_density2)
    deallocate(nl_tperp1)

  end subroutine deallocate_all

  !========================================================================================

  subroutine pertb
    !
    ! This subroutine inserts the perturbation.
    !
    use itg_data
    use fields
    use gryffin_grid, only: malias, nalias
    use gryffin_grid, only: mrr
    use nlps, only: f_reality, dev_reality
    use devObject, only: transfer_c4, use_gpu

    real a,b,p00, t00
    integer m,n

    potential = 0.
    density   = 0.

    do n = 1,nalias
       do m = 1,malias
          !          if (mrr(m)==minput .and. (n==2 .or. n==nalias)) then   ! NAIL:  CHANGE
          if (mrr(m)==minput .and. n==1) then                     ! NAIL:  CHANGE
             p00 = pinput1
             t00 = pinput2
          else
             p00 = 0.
             t00 = 0.
          end if
          density(m,n)=cmplx(p00,0.)
          tperp(m,n)=cmplx(t00,0.)
          !                density(l,m,n,i)=cmplx(p00,-p00)
       enddo
    enddo

    do n=1,nalias/4
       do m=1,malias/4
          call random_number (a)
          call random_number (b)
          a=a*pmag
          b=b*pmag
          density(m,n)=density(m,n) + cmplx(a,b)
       enddo
    enddo

    !_______________Nail's block__________________________!transfer initial data from cpu to gpu
    !
    if (use_gpu) then
       call transfer_c4(potential,dv_potential,.true.)   !
       call transfer_c4(density,dv_density,.true.)       !
       call transfer_c4(tperp,dv_tperp,.true.)           !
    end if
    !____________________________________________________!


    !           
    ! Enforce the reality condition for the ky=0 components:
    ! 
    if (debug) write (*,*) 'About to call reality'
    if (use_gpu) then
       call dev_reality (dv_density) 
       call dev_reality (dv_tperp)
    else
       call f_reality (density) 
       call f_reality (tperp)
    end if
    if (debug) write (*,*) 'Finished call to reality'


  end subroutine pertb
!.....................................................................................
  subroutine wpunch

    use itg_data, only: nprint
    use fields, only: psp, timo

    integer i

    open(unit=31,file='output',status='unknown',form='formatted')

    do i=1,nprint
       write (31,*) timo(i), phitot(i)
    end do

    close(31)

  end subroutine wpunch

end program itg

