module nlps

  use gputest  ! for gpu test only

!
! "NonLinear PseudoSpectral" BD
! Given the fields a, b, ka, and kb this subroutine calculates the 
! Poisson bracket
!		{a,b} = da/dx db/dy - da/dy db/dx.
!
! The fields arrive in (ky,kz) space and must be transformed
! with the appropriate derivatives into (x,y) space.
!


  use constants, only: debug
  use devObject, only: devVar, devf_fft, devf_bfft, devf_hadamardf
  use itg_device_func
  implicit none

  private
  public :: init_nlps, nlpsc, dev_reality, f_reality
  
  interface nlpsc
     module procedure dev_nlpsc, cpu_nlpsc
  end interface

!_______Nail's block____________________________________________________________________
                                                                                        !
  public :: fftplan,dv_scalefac,dv_ikx,dv_iky,dv_kxa2,dv_kya2,dv_kxb2,dv_kyb2           !
!_______________________________________________________________________________________!


  complex, dimension(:), allocatable :: ikx, iky
  complex, dimension(:,:), allocatable, save :: kxa2, kya2, kxb2, kyb2  
  real, dimension(:,:), allocatable :: scalefac
  integer*8, save :: plan_f, plan_b
  
  public :: scalefac,ikx,iky,kxa2,kya2,kxb2,kyb2             ! added for further deallocation
  



!_______Nail's block____________________________________________ 
                                                                !
  type(devVar) dv_ikx, dv_iky                                   !! Nail: these variables are purely imaginary and can be stored as reals
  type(devVar) dv_kxa2, dv_kya2, dv_kxb2, dv_kyb2               !
  type(devVar) dv_scalefac                                      !
  integer, save :: fftplan                                            !! the same plan is used for forward and backward in cufft       
  real, allocatable :: h_temp(:,:)                              !! temporary variable
!_______________________________________________________________!  

    
contains

  subroutine init_nlps 
    
    use devObject, only: alloc_dv, transfer_r4, devf_zeros, devf_fftplan, use_gpu
    use constants, only: ii
    use grid, only: mddp, madd, nddp, nadd, malias, nalias
    use grid, only: rkx, rky
    
    integer, parameter :: fftw_estimate   =  0
    integer, parameter :: fftw_measure    =  1
    integer, parameter :: fftw_in_place   =  8
    integer, parameter :: fftw_use_wisdom = 16
    integer :: j, m, n

    real :: fac

    if(allocated(scalefac)) deallocate(scalefac) ; allocate (scalefac(malias, nalias)) ; scalefac = 0.
        
    fac = 1./(real(malias)*real(nalias))

    do n=1,nddp
       do m=1,mddp
          scalefac(m,n) = fac
       enddo
       do m=madd+mddp+1,malias
          scalefac(m,n) = fac
       enddo
    enddo
    
    do n=nadd+nddp+1,nalias
       do m=1,mddp
          scalefac(m,n) = fac
       enddo

       do m=madd+mddp+1,malias
          scalefac(m,n) = fac
       enddo
    enddo
    
    if (use_gpu) then

       !_______________Nail's block_________________!
       !
       dv_scalefac=alloc_dv('real',malias,nalias)   !
       call transfer_r4(scalefac,dv_scalefac,.true.)!
       
       dv_ikx=alloc_dv('real',malias,nalias)        !    Nail: ikx is real, while it will be treated as purely imaginary
       dv_iky=alloc_dv('real',malias,nalias)        !    Nail: iky is real, while it will be treated as purely imaginary 
       !
       allocate(h_temp(malias,nalias))              !
       !
       do n=1,nalias                                !    Nail: extend arrays from 1d to 2d (to speedup GPU)
          h_temp(:,n) = rkx(n)                      !
       enddo                                        !
       !
       call transfer_r4(h_temp,dv_ikx,.true.)       !
       !
       do m=1,malias                                !
          h_temp(m,:)=rky(m)                        !
       end do                                       !
       !
       call transfer_r4(h_temp,dv_iky,.true.)       !
       
       deallocate(h_temp)                           !
                                                    !
       !____________________________________________!

    else
       if(allocated(ikx)) deallocate(ikx) ;  allocate (ikx(nalias))  ;  ikx = 0.
       if(allocated(iky)) deallocate(iky) ;  allocate (iky(malias))  ;  iky = 0.
       
       do n=1,nalias
          ikx(n) = cmplx(0.,rkx(n))
       end do
       
       do m=1,malias
          iky(m) = cmplx(0.,rky(m))
       end do
    end if


    if (use_gpu) then
       !___________Nail's block__________________________________________________
       !
       dv_kxa2=alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_kxa2) !
       dv_kya2=alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_kya2) !
       dv_kxb2=alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_kxb2) !
       dv_kyb2=alloc_dv('complex',malias,nalias)  ; call devf_zeros(dv_kyb2) !
       !_________________________________________________________________________!
    else
       if(allocated(kxa2)) deallocate(kxa2) ; allocate (kxa2(malias, nalias)) ; kxa2 = 0.
       if(allocated(kya2)) deallocate(kya2) ; allocate (kya2(malias, nalias)) ; kya2 = 0.
       if(allocated(kxb2)) deallocate(kxb2) ; allocate (kxb2(malias, nalias)) ; kxb2 = 0.
       if(allocated(kyb2)) deallocate(kyb2) ; allocate (kyb2(malias, nalias)) ; kyb2 = 0.
    end if

!___________Nail's block___________________________________________________

    fftplan = devf_fftplan(dv_kxa2)                                        !
!__________________________________________________________________________!


  end subroutine init_nlps



!===========================================================Nail's GPU analog

  subroutine dev_nlpsc(isave, a, b, nl_term)

    type(devVar), intent(in)  :: a, b
    type(devVar), intent(out) :: nl_term
    integer, intent (in) :: isave

! Don't recalculate FFT's unnecessarily!
!
! isave determines which fields are new for this call:
! isave=0 -> both fields are new
! isave=1 -> "a" fields are the same as last time nlps was called
! isave=2 -> "b" fields are the same as last time nlps was called 
!

! Calculate the d/dy and d/dx terms:

    if(isave /= 1) then                                                  

        call devcust_itg_hadam2(a,dv_ikx,dv_kxa2,dv_iky,dv_kya2)  
        call devf_fft(dv_kxa2,fftplan)                     
        call devf_fft(dv_kya2,fftplan)

    endif

    if(isave /= 2) then

        call devcust_itg_hadam2(b,dv_ikx,dv_kxb2,dv_iky,dv_kyb2)  
        call devf_fft(dv_kxb2,fftplan)
        call devf_fft(dv_kyb2,fftplan)

    endif

! Perform the multiplications:

    call devcust_itg_det2(nl_term,dv_kxa2,dv_kxb2,dv_kya2,dv_kyb2)          

! Transform result back to k space

    call devf_bfft(nl_term,fftplan)                     

! Scale and dealias:

    call devf_hadamardf(nl_term,nl_term,dv_scalefac)     

  end subroutine dev_nlpsc

  subroutine cpu_nlpsc(isave, a, b, nl_term)
  
    use constants, only: ii
    use grid, only: rkx, rky, malias, nalias
      
    complex, dimension(:,:), intent(in)  :: a, b
    complex, dimension(:,:), intent(out) :: nl_term
    
    integer :: m, n, isave

! Don't recalculate FFT's unnecessarily!
!
! isave determines which fields are new for this call:
! isave=0 -> both fields are new
! isave=1 -> "a" fields are the same as last time nlps was called
! isave=2 -> "b" fields are the same as last time nlps was called 
!

! Calculate the d/dy and d/dx terms:

    if(isave /= 1) then
       do n=1,nalias
          kxa2(:,n) = ikx(n) * a(:,n)
       end do

       do m=1,malias
          kya2(m,:) = iky(m) * a(m,:)
       end do
    endif
    
    if(isave /= 2) then
       do n=1,nalias
          kxb2(:,n) = ikx(n) * b(:,n)
       end do

       do m=1,malias
          kyb2(m,:) = iky(m) * b(m,:)
       end do
    endif
     
! Transform the vectors i*kx*a, i*kx*b, i*ky*b and i*ky*a into x space:

    if(isave /= 1) then
       !call fftwnd_f77_one(plan_f, kxa2, kxa2)
       !call fftwnd_f77_one(plan_f, kya2, kya2)
       call FFT2_C2C_D (kxa2,kxa2,malias,nalias,1)
       call FFT2_C2C_D (kya2,kya2,malias,nalias,1)
    endif
    
    if(isave /= 2) then
       !call fftwnd_f77_one(plan_f, kxb2, kxb2)
       !call fftwnd_f77_one(plan_f, kyb2, kyb2)
       call FFT2_C2C_D (kxb2,kxb2,malias,nalias,1)
       call FFT2_C2C_D (kyb2,kyb2,malias,nalias,1)
    endif
 
! Perform the multiplications
     
    nl_term = kxa2 * kyb2 - kxb2 * kya2 

! Transform result back to k space

    !call fftwnd_f77_one (plan_b, nl_term, nl_term)
     call FFT2_C2C_D (nl_term,nl_term,malias,nalias,0)

! Scale and dealias: 
    !nl_term = nl_term * scalefac
    nl_term = (malias*nalias)*(nl_term * scalefac)

  end subroutine cpu_nlpsc

 subroutine dev_reality (a)

   use devObject, only: devVar, devf_fft, devf_ifft, devf_partofcmplx

   type(devVar) :: a
   
   call devf_fft(a,fftplan)
   call devf_partofcmplx('real',a) 
   call devf_ifft(a,fftplan)
   
 end subroutine dev_reality

 subroutine f_reality (a)
   use grid, only: malias, nalias
   complex, dimension(:,:) ::  a
   integer :: m, n 
   
   if (debug) then
      do n=1,nalias
         do m=1,malias
            write (*,*) real(a(m,n)), aimag(a(m,n))
         end do
         write (*,*) 
      end do
      write (*,*) plan_f, plan_b
   end if
   !call fftwnd_f77_one (plan_f, a, a)
   call FFT2_C2C_D (a,a,malias,nalias,1)
   
   !    if (debug) write (*,*) 'called fft once'
   a = real(a)
   !call fftwnd_f77_one (plan_b, a, a)
   call FFT2_C2C_D (a,a,malias,nalias,0)
   !a = a * scalefac                        ! Nail: this also provides paddding, not only scaling: removed as forward and inverse is enough for reality
   
   if (debug) then
      do n=1,nalias
         do m=1,malias
            write (*,*) m,n,real(a(m,n)), aimag(a(m,n))
         end do
         write (*,*) 
      end do
   end if
   
 end subroutine f_reality

end module nlps

